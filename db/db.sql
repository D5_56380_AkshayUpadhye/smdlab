create table Emp(
    empid integer primary key auto_increment,
    name varchar(50),
    salary double,
    age integer
);

insert into Emp values(default,"Vinayak",15500,226);
insert into Emp values(default,"Raju",17000,25);
insert into Emp values(default,"Pankaj",16000,27);